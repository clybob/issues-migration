#!/usr/bin/env python
from __future__ import print_function
from requests.compat import quote as urlquote

import getpass
import github3
import requests
import sys

try:
    from configparser import SafeConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser

try:
    # If we're on Python3, raw_input no longer exists
    user_input = raw_input
except NameError:
    user_input = input

VERSION = '0.0.1'
CONFIG_FILE = 'config.ini'
ISSUE_BODY = b"""[Issue #{i.number}]({i.html_url}) was originally submitted \
by [@{i.user}]({i.user.html_url}) on {i.created_at}. It was last updated at \
{i.updated_at}.

------

{i.body}
"""
COMMENT_BODY = b"""[This comment]({c.html_url}) was originally made by \
[@{c.user}]({c.user.html_url}) on {c.created_at}. It was last updated at \
{c.updated_at}.

------

{c.body}
"""


class GitLabSession(requests.Session):
    gitlab_host = 'https://gitlab.com'

    def __init__(self):
        super(GitLabSession, self).__init__()
        self.headers.update({'Connection': 'close'})

    def build_url(self, *args):
        url_args = [self.gitlab_host] + list(args)
        return '/'.join(url_args)

    def set_token(self, token):
        self.headers.update({'PRIVATE-TOKEN': token})


def get_config():
    config = SafeConfigParser()
    config.read(CONFIG_FILE)
    return config


def get_username(service, default_username=None):
    default_username = default_username or getpass.getuser()
    return user_input(
        'Please enter your {0} username [default: {1}]: '.format(
            service, default_username
            )
        ) or default_username


def get_password(service):
    return getpass.getpass(
        'Please enter your {0} password: '.format(service)
        )


def get_credentials(service):
    return (get_username(service), get_password(service))


def get_project_id(session, project_name):
    project_name = urlquote(project_name, safe='')
    gitlab_url = session.build_url('api', 'v3', 'projects', project_name)
    resp = session.get(gitlab_url)
    if resp.ok:
        return resp.json().get('id')


def create_github_token():
    # This doesn't need to be public to the script
    def two_factor_auth_cb():
        prompt_str = 'Please enter your Two Factor Authentication token: '
        code = ''
        while not code:
            code = user_input(prompt_str)
        return code

    username, password = get_credentials('GitHub')
    g = github3.login(username, password,
                      two_factor_callback=two_factor_auth_cb)
    print(u'Requesting a GitHub authorization token.')
    authorization = g.authorize(
        None, None, scopes=['repo'], note='Issue migration tool',
        note_url='https://gitlab.com/sigmavirus24/issues-migration'
        )
    print(u'Storing the token in `.ghtoken`.')
    with open('.ghtoken', 'w+') as fd:
        fd.write(authorization.token)

    return authorization.token


def create_gitlab_token():
    # Get the authentication credentials
    username, password = get_credentials('GitLab')
    # Set up the session
    session = GitLabSession()
    default_host = session.gitlab_host
    # Get the preferred GitLab host
    host = user_input(
        'Preferred GitLab host [default: {0}]'.format(default_host)
        ) or default_host
    # Set the host on the session
    session.gitlab_host = host
    # Build the session URL to get a token
    url = session.build_url('api', 'v3', 'session')
    data = {'email': username, 'password': password}

    print(u'Retrieving your GitLab private token.')
    r = session.post(url, data)
    # Until we retrieve the
    while not r.status_code == 201:
        print(u'Invalid credentials.')
        # Allow them to re-enter their username but provide the old one as a
        # default
        username = get_username('GitLab', username)
        # Allow them to re-enter their password
        password = get_password('GitLab')
        data = {'email': username, 'password': password}
        # Try again
        r = session.post(url, data)

    print(u'Storing the token in `.gltoken`')
    # Retrieve the token
    token = r.json()['private_token']
    config = get_config()
    if not config.has_section('gitlab'):
        config.add_section('gitlab')

    config.set('gitlab', 'token', token)
    config.set('gitlab', 'host', session.gitlab_host)
    with open(CONFIG_FILE, 'w+') as fd:
        config.write(fd)

    # Return the token and the session
    return session


def create_gitlab_session():
    config = get_config()
    if not config.has_section('gitlab'):
        session = create_gitlab_token()
    else:
        session = GitLabSession()
        token = config.get('gitlab', 'token')
        session.set_token(token)
    return session


def create_github_session():
    config = get_config()
    if not config.has_section('github'):
        token = create_github_token()
    else:
        token = config.get('github', 'token')

    return github3.GitHub(token=token)


def create_issue(gl, issue, gitlab_issues, project_id):
    print(u'Migrating {0}: {1}'.format(issue.number, issue.title))
    r = gl.post(
        gitlab_issues,
        data={
            'id': project_id,
            'title': issue.title,
            'description': format_issue_body(issue),
            }
        )
    print(u'  Response {0} from GitLab'.format(r.status_code))
    return r.json()


def create_issue_note(gl, comment, url, project_id, issue_id):
    r = gl.post(
        url,
        data={
            'id': project_id,
            'issue_id': issue_id,
            'body': format_comment_body(comment)
            }
        )
    print(u'  Response {0} from GitLab'.format(r.status_code))


def close_issue(gl, issue_json, project_id):
    issue_id = issue_json['id']
    url = gl.build_url('api', 'v3', 'projects', str(project_id), 'issues',
                       str(issue_id))
    print(u'  Auto-closing issue {0} on GitLab'.format(issue_json['iid']))
    gl.put(url, data={
        'id': project_id,
        'issue_id': issue_id,
        'state_event': 'close'
        })


def copy_comments(gl, issue_json, project_id, gh_issue):
    print(u'Migrating comments on issue #{0}'.format(gh_issue.number))
    i = 1
    issue_id = issue_json['id']
    url = gl.build_url('api', 'v3', 'projects', str(project_id), 'issues',
                       str(issue_id), 'notes')
    for comment in gh_issue.iter_comments():
        print(u'  Creating comment {0}/{1}'.format(i, gh_issue.comments))
        create_issue_note(gl, comment, url, project_id, issue_id)
        i += 1


def format_issue_body(issue):
    return ISSUE_BODY.decode('utf-8').format(i=issue)


def format_comment_body(comment):
    return COMMENT_BODY.decode('utf-8').format(c=comment)


def usage():
    script_name = sys.argv[0]
    print(u'Usage: {0} github-username/github-repository '
          'gitlab-username/gitlab-project\n'.format(script_name))
    print(u'You are currently using version {0}\n'.format(VERSION))
    sys.exit(1)


def main():
    if not (sys.argv[1:] and len(sys.argv[1:]) == 2):
        usage()

    gh = create_github_session()
    gl = create_gitlab_session()
    project_id = get_project_id(gl, sys.argv[-1])
    user, repo = sys.argv[1].split('/')
    gitlab_issues = gl.build_url('api', 'v3', 'projects', str(project_id),
                                 'issues')

    kwargs = {'sort': 'created', 'direction': 'asc', 'state': 'all'}
    for issue in gh.iter_repo_issues(user, repo, **kwargs):
        i = create_issue(gl, issue, gitlab_issues, project_id)
        if issue.is_closed():
            close_issue(gl, i, project_id)
        copy_comments(gl, i, project_id, issue)


if __name__ == '__main__':
    main()
